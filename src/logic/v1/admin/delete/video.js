module.exports = class extends think.Logic {
    async __before() {
        const pathName = this.ctx.path.slice(this.ctx.path.lastIndexOf('/') + 1);
        this.ctx.state.data = JSON.stringify(this.ctx.post()) !== '{}' ? this.ctx.post() : this.ctx.param();
        this.ctx.state.pathName = pathName;
    }

    async indexAction() {

        try {
            const checkUser = await this.model('admin/admin').checkUserToken({
                token: this.ctx.state.data.data.token,
                admin_id: this.ctx.state.data.data.admin_id
            });
            if (think.isEmpty(checkUser)) {
                throw false
            }
        } catch (err) {
            return false
        }

        delete this.ctx.state.data.data.token;
        delete this.ctx.state.data.data.admin_id

    }
};