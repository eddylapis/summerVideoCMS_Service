const Base = require(think.APP_PATH + '/controller/base.js');
const fs = require('fs');
const path = require('path');
const rename = think.promisify(fs.rename, fs);
module.exports = class extends Base {
    async indexAction() {
        const file = this.file('video');
        const extFileName = file.name.slice(file.name.lastIndexOf('.'));
        if (file) {
            const filepath = path.join(think.ROOT_PATH, 'www/static/upload/video/' + file.hash + extFileName);
            think.mkdir(path.dirname(filepath));
            await rename(file.path, filepath);
            const url = this.ctx.headers.host.includes('http') ? this.ctx.headers.host : '//' + this.ctx.headers.host;
            this.ctx.state.result = {
                srcName: file.hash + extFileName,
                srcLocation: url + '/static/upload/video/'
            }
        } else {
            throw false
        }
    };
};