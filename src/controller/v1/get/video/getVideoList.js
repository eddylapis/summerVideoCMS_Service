const Base = require(think.APP_PATH + '/controller/base.js');
module.exports = class extends Base {

    async indexAction() {
        this.ctx.state.result = this.model('video/video').getList(this.ctx.state.data);
    };

};