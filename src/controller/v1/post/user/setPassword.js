const Base = require(think.APP_PATH + '/controller/base.js');
module.exports = class extends Base {
    async indexAction() {
        let {verify_id, emailCode, email, password} = this.ctx.state.data.data;
        if (think.isEmpty(await this.model('verify/verify').checkMailcode({verify_id, emailCode, email}))) {
            throw 500
        } else {
            this.model('user/user').setPassword({email,password});
            this.ctx.state.result = 1
        }
    };
};