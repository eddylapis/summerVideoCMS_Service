module.exports = class extends think.Controller {
    async __before() {

    }

    async __after() {
        this.body = {
            [this.ctx.state.pathName]: await this.ctx.state.result
        }
    }
};
