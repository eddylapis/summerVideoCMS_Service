const numCPUs = require('os').cpus().length;
module.exports = {
    workers: numCPUs * 2,
    port: 7001
};
